"use strict"
module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    watch:
      javascript:
        files: [
          "assets/javascripts/**/*{coffee,js}"
        ]
        tasks: ["rig"]
      sass:
        files: [
          "assets/stylesheets/**/*{sass,scss}"
        ]
        tasks: ["compass:dev"]

    compass:
      dev:
        options:
          require: "susy"
          outputStyle: "nested"
          sassDir: "assets/stylesheets"
          cssDir: "assets/dist/stylesheets"
      dist:
        options:
          require: "susy"
          outputStyle: "compressed"
          sassDir: "assets/stylesheets"
          cssDir: "assets/dist/stylesheets"

    # Use rig only for scripts
    rig:
      compile:
        files:
          "assets/dist/javascripts/application.js": ["assets/javascripts/application.js"]
          "assets/dist/javascripts/vendor.js": ["assets/javascripts/vendor.js"]

    clean:
      src: ["assets/dist/javascripts/*", "assets/dist/stylesheets/*"]

    uglify:
      options:
        mangle: false
      dist:
        files:
          "assets/dist/javascripts/application.js": ["assets/dist/javascripts/application.js"]
          "assets/dist/javascripts/vendor.js": ["assets/dist/javascripts/vendor.js"]

    modernizr:
      "devFile" : "assets/components/modernizr/modernizr.js"
      "outputFile" : "assets/dist/javascripts/modernizr-custom.js"

  # These plugins provide necessary tasks.
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-rigger"
  grunt.loadNpmTasks "grunt-modernizr"
  
  # Default task.
  grunt.registerTask "default", ["compass"]
  grunt.registerTask "dist", ["clean", "rig", "uglify", "compass:dist", "modernizr"]

