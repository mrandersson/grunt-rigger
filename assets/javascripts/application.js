/**
 * Javascript manifest
 */

// When including coffee files, you have to add the .coffee extension
//= src/example.coffee
//
// When including regular javascript files, include them without the extension
//= src/example
